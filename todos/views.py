from django.shortcuts import render
from .models import TodoList

# Create your views here.


def todo_list_list(request):
    lists=TodoList.objects.all()
    context = {
        "TodoItem": lists,
        }
    return render(request, "todos/list.html", context)
